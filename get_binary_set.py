import os

archivo = open("input.txt", "r")
c = 0
vectors = list()
n = int


def multiplication(v_aux):
    result = []
    for i in v_aux:
        if(i == "0.5"):
            result.append(1)
        else:
            result.append((int(i)) * 2)
    return result


# READ FILE
for l in archivo.readlines():
    c = c + 1
    if(c == 1):
        n = l
        n = int(n.replace('\n', ''))
    if(c >= 3):
        v_aux = l.split(" ")
        thelast = v_aux.pop(-1)
        v_aux.append(thelast.replace('\n', ''))
        v_aux = multiplication(v_aux)
        vectors.append(v_aux)
print("SET S = " + str(vectors))
archivo.close()
# END

vectorAux = []

result = set()

def caseOne():
     if (len(vectorAux) == 0):
         vectorAux.append("1 ")
         vectorAux.append("0 ")
         vectorAux.append("0 ")
         vectorAux.append("1 ")
     else:
         for i in range(len(vectorAux)):
             vectorAux.append(vectorAux[i] + "1 ")
             vectorAux.append(vectorAux[i] + "0 ")


def caseZero():
    if (len(vectorAux) == 0):
        vectorAux.append("0 ")
        vectorAux.append("0 ")
    else:
        for i in range(len(vectorAux)):
            vectorAux[i] = vectorAux[i] + "0 "


def caseTwo():
    if(len(vectorAux) == 0):
        vectorAux.append("1 ")
        vectorAux.append("1 ")
    else:
        for i in range(len(vectorAux)):
            vectorAux[i] = vectorAux[i] + "1 "


def filterVector():
    for x in vectorAux:
        if(len(x) == (n+n)): # n + n espacios
            result.add(x)
    return result


def getSet(vector):
    for x in vector:
      x_str = str(x)
      if(x_str == "0"):
          caseZero()
      if(x_str == "1"):
          caseOne()
      if (x_str == "2"):
          caseTwo()

    filterVector()

    return result

for v in vectors:
    getSet(v)
    vectorAux = []

print(result)

def splitSet(param_set):
    file = open("B_optimized.txt","w")
    listset = list(param_set)
    print("List: ",len(listset))

    for i in range(len(listset)):
        vector = listset[i]
        file.write(vector + os.linesep)
    file.close()

splitSet(result)
