

import os


def pedirNumeroEntero(pedidoString):
	correcto= False
	while(not correcto):
		try:
			num= int(input(pedidoString))
			correcto=True
		except ValueError:
			print('Error, debe de introducir un numero entero')
	return num



pedir_N="Introduce un numero entero para n: "
pedir_cardinalS="Introduce un numero entero para cardinalS: "

n=pedirNumeroEntero(pedir_N)
print('N= ', n)


#####  ARCHIVO BINARIO COMPLETO #####
def get_cardinalDeBcompleto(n):
    cardinalB = 1
    for i in range(n):
        cardinalB = cardinalB * 2

    print("cardinalB: ", cardinalB)
    return cardinalB


cardinalB = get_cardinalDeBcompleto(n)


def cambioValor(valor):
    if (valor == 0):
        return 1
    else:
        return 0


## SIEMPRE LA PRIMERA MITAD ES DE 0
## SIEMPRE LA SEGUNDA MITAD ES DE 1
## cantFilas= cardinalB
## cantColumnas= n
def crear_matriz_B_completa(cardinalB, n):
    mitad_actual = int(cardinalB / 2)
    is_enPrimeraMitad = True

    ##CREO LA MATRIZ VACÍA
    matriz = []
    for x in range(cardinalB):
        matriz.append([0] * n)
    # print(matriz)

    ##RECORRO LA MATRIZ Y LO PONGO EN BINARIO
    for i in range(n):  # columnas
        print("mitad_actual: ", mitad_actual)
        valor = 0
        aux = 0
        for j in range(cardinalB):  # filas
            if (aux == mitad_actual):
                valor = cambioValor(valor)
                matriz[j][i] = valor
                aux = 0
            else:
                matriz[j][i] = valor
            aux = aux + 1
        mitad_actual = int(mitad_actual / 2)

    print(matriz)

    return (matriz)


## CREO LA MATRIZ BINARIA COMPLETA
matriz = crear_matriz_B_completa(cardinalB, n)


def guardarMatrizEnTXT(matriz, cardinalB, n):
    file = open("inputB_completo.txt", "w")
    ##RECORRO LA MATRIZ Y LO PONGO EN BINARIO
    for i in range(cardinalB):  # FILAS
        vector = ""
        for j in range(n):  # COLUMNAS
            vector = vector + str(matriz[i][j]) + " "
        file.write(vector + os.linesep)
    file.close()


guardarMatrizEnTXT(matriz, cardinalB, n)
